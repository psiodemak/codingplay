﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using CodingPlayDomain;

namespace WcfGraphContract
{
    /// <summary>
    /// Contract for graph service. Is used by GUI client to draw graph and calculate path.
    /// </summary>
    [ServiceContract]
    public interface IWcfGraphContract
    {
        [OperationContract]
        string TestGraphService();

        [OperationContract]
        List<string> CalculateShortestPath(string nodeIdFrom, string nodeIdTo);

        [OperationContract]
        List<NodeEntity> GetAllNodes();

        [OperationContract]
        List<NodeRelationEntity> GetAllRelations();
    }
}
