﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfDataLoaderShared
{
    /// <summary>
    /// Result from xml parser.
    /// </summary>
    public class ParseXmlResult
    {
        public ParseXmlResult()
        {
            Nodes = new List<NodeEntity>();
            Relations = new List<NodeRelationEntity>();
        }

        public List<NodeEntity> Nodes { get; private set; }
        public List<NodeRelationEntity> Relations { get; private set; }
    }
}
