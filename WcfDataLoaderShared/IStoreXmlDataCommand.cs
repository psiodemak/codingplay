﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WcfDataLoaderShared
{
    /// <summary>
    /// Interface for storing data command.
    /// </summary>
    public interface IStoreXmlDataCommand
    {
        void StoreXmlData(XElement xmlData);
    }
}
