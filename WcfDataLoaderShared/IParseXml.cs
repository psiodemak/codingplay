﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WcfDataLoaderShared
{
    /// <summary>
    /// Interface for xml parser.
    /// </summary>
    public interface IParseXml
    {
        ParseXmlResult Execute(XElement xRoot);
    }
}
