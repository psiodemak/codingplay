﻿using GraphProcessor;
using GraphShared;
using NUnit.Framework;

namespace GraphProcessorTests
{
    [TestFixture]
    public class GraphProcessorTests
    {
        [Test]
        public void FindTrail_NoPath()
        {
            Graph graph = new Graph();
            GraphNode node0 = graph.AddNode(0);
            GraphNode node1 = graph.AddNode(1);
            GraphNode node2 = graph.AddNode(2);

            node0.AddNeighbour(node1);

            FindPath findPath = new FindPath();
            var result = findPath.FindTrail(graph, 0, 2);
            Assert.IsTrue(result.Count == 0);
        }

        [Test]
        public void FindTrailFor3Elements()
        {
            Graph graph = new Graph();

            GraphNode node0 = graph.AddNode(0);
            GraphNode node1 = graph.AddNode(1);
            GraphNode node2 = graph.AddNode(2);

            node0.AddNeighbour(node1);
            node1.AddNeighbour(node2);

            FindPath findPath = new FindPath();
            var result = findPath.FindTrail(graph, 0, 2);
            Assert.IsTrue(result.Count == 3);

            result = findPath.FindTrail(graph, 2, 0);
            Assert.IsTrue(result.Count == 3);

            result = findPath.FindTrail(graph, 1, 2);
            Assert.IsTrue(result.Count == 2);
        }

        [Test]
        public void FindTrailFor4Elements()
        {
            Graph graph = new Graph();

            GraphNode node0 = graph.AddNode(0);
            GraphNode node1 = graph.AddNode(1);
            GraphNode node2 = graph.AddNode(2);
            GraphNode node3 = graph.AddNode(3);

            node0.AddNeighbour(node1);
            node0.AddNeighbour(node2);

            node1.AddNeighbour(node3);
            node1.AddNeighbour(node2);

            node2.AddNeighbour(node2);

            FindPath findPath = new FindPath();
            var result = findPath.FindTrail(graph, 0, 3);
            Assert.IsTrue(result.Count == 3);

            result = findPath.FindTrail(graph, 2, 2);
            Assert.IsTrue(result.Count == 0);

            result = findPath.FindTrail(graph, 2, 3);
            Assert.IsTrue(result.Count == 3);
        }

        [Test]
        public void FindTrailFor6Elements()
        {
            Graph graph = new Graph();

            GraphNode node0 = graph.AddNode(0);
            GraphNode node1 = graph.AddNode(1);
            GraphNode node2 = graph.AddNode(2);
            GraphNode node3 = graph.AddNode(3);
            GraphNode node4 = graph.AddNode(4);
            GraphNode node5 = graph.AddNode(5);

            node0.AddNeighbour(node1);
            node0.AddNeighbour(node2);

            node1.AddNeighbour(node3);
            node1.AddNeighbour(node2);

            node2.AddNeighbour(node2);
            node2.AddNeighbour(node4);

            node3.AddNeighbour(node4);
            node3.AddNeighbour(node5);

            node4.AddNeighbour(node5);

            FindPath findPath = new FindPath();
            var result = findPath.FindTrail(graph, 0, 5);
            Assert.IsTrue(result.Count == 4);

            result = findPath.FindTrail(graph, 2, 5);
            Assert.IsTrue(result.Count == 3);
        }
    }
}
