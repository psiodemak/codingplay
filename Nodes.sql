﻿CREATE TABLE [dbo].[Nodes] (
    [NodeId] NVARCHAR (50)  NOT NULL,
    [Label]  NVARCHAR (200) NOT NULL,
    PRIMARY KEY CLUSTERED ([NodeId] ASC)
);