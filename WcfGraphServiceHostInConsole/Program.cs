﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfGraphServiceHostInConsole
{
    /// <summary>
    /// Host of wcf graph service.
    /// </summary>
    class Program
    {
        private static ServiceHost _host;

        /// <summary>
        /// Host of wcf graph service. Host is based on .config file.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            StartService();

            Console.WriteLine("Wcf Graph Service is running. Press any key to finish....");
            Console.ReadKey();

            CloseService();
        }

        private static void StartService()
        {
            _host = new ServiceHost(typeof(WcfGraphService.WcfGraphService));
            _host.Open();
        }

        private static void CloseService()
        {
            if (_host.State != CommunicationState.Closed)
            {
                _host.Close();
            }
        }
    }
}
