﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphShared
{
    /// <summary>
    /// Interface for graph finder.
    /// </summary>
    public interface IGraphTrailFinder
    {
        List<int> FindTrail(Graph graph, int fromId, int toId);
    }
}
