﻿using System.Collections.Generic;

namespace GraphShared
{
    /// <summary>
    /// Graph node.
    /// </summary>
    public class GraphNode
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">
        /// Id of node
        /// </param>
        public GraphNode(int id)
        {
            Id = id;
            AdjacentNodes = new HashSet<GraphNode>(); // hashset to avoid duplicates
        }

        /// <summary>
        /// Node id
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Adjacent nodes
        /// </summary>
        public HashSet<GraphNode> AdjacentNodes
        {
            get; private set;
        }

        /// <summary>
        /// Create relation between nodes. Becouse graph is undirected create two relations.
        /// </summary>
        /// <param name="node"></param>
        public void AddNeighbour(GraphNode node)
        {
            AdjacentNodes.Add(node);
            node.AdjacentNodes.Add(this);
        }
    }
}
