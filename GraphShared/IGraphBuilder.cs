﻿namespace GraphShared
{
    /// <summary>
    /// Interface for graph builder.
    /// </summary>
    public interface IGraphBuilder
    {
        Graph PrepareGraph();

        int GetGraphIdForNodeId(string nodeId);
        string GetNodeIdForGraphId(int graphId);
    }
}
