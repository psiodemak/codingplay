﻿using System.Collections.Generic;

namespace GraphShared
{
    /// <summary>
    /// Graph. It's build in Builder and analyzed in FindPath.
    /// </summary>
    public class Graph
    {
        public Graph()
        {
            Nodes = new List<GraphNode>();
        }

        public List<GraphNode> Nodes { get; private set; }

        public GraphNode AddNode(int id)
        {
            GraphNode node = new GraphNode(id);
            Nodes.Add(node);

            return node;
        }
    }
}
