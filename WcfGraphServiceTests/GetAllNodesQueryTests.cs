﻿using CodingPlayDomain;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphService;

namespace WcfGraphServiceTests
{
    [TestFixture]
    public class GetAllNodesQueryTests
    {
        [Test]
        public void GetAllNodesQuery_Test()
        {
            IGraphRepository repo = A.Fake<IGraphRepository>();
            GetAllNodesQuery query = new GetAllNodesQuery(repo);
            var result = query.GetAllNodes();

            A.CallTo(() => repo.GetAllNodes()).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}
