﻿using CodingPlayDomain;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphService;

namespace WcfGraphServiceTests
{
    [TestFixture]
    public class GetAllRelationsQueryTests
    {
        [Test]
        public void GetAllRelationsQuery_Test()
        {
            IGraphRepository repo = A.Fake<IGraphRepository>();
            GetAllRelationsQuery query = new GetAllRelationsQuery(repo);
            var result = query.GetAllRelations();

            A.CallTo(() => repo.GetAllRelations()).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}
