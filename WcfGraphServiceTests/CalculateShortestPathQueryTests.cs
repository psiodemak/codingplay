﻿using FakeItEasy;
using GraphProcessor;
using GraphShared;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphService;

namespace WcfGraphServiceTests
{
    [TestFixture]
    public class CalculateShortestPathQueryTests
    {
        [Test]
        public void CalculateShortestPath_Test()
        {
            IGraphBuilder mockedBuilder = PrepareBuilder();

            CalculateShortestPathQuery query = new CalculateShortestPathQuery(mockedBuilder, new FindPath());
            var result = query.CalculateShortestPath("node0", "node5");
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 4);
            Assert.IsTrue(result.Contains("node0"));
            Assert.IsTrue(result.Contains("node1"));
            Assert.IsTrue(result.Contains("node3"));
            Assert.IsTrue(result.Contains("node5"));
        }

        private IGraphBuilder PrepareBuilder()
        {
            Graph graph = PrepareGraph();

            IGraphBuilder mockedBuilder = A.Fake<IGraphBuilder>();
            A.CallTo(() => mockedBuilder.PrepareGraph()).Returns(graph);

            A.CallTo(() => mockedBuilder.GetGraphIdForNodeId("node0")).Returns(0);
            A.CallTo(() => mockedBuilder.GetGraphIdForNodeId("node5")).Returns(5);

            A.CallTo(() => mockedBuilder.GetNodeIdForGraphId(0)).Returns("node0");
            A.CallTo(() => mockedBuilder.GetNodeIdForGraphId(1)).Returns("node1");
            A.CallTo(() => mockedBuilder.GetNodeIdForGraphId(2)).Returns("node2");
            A.CallTo(() => mockedBuilder.GetNodeIdForGraphId(3)).Returns("node3");
            A.CallTo(() => mockedBuilder.GetNodeIdForGraphId(4)).Returns("node4");
            A.CallTo(() => mockedBuilder.GetNodeIdForGraphId(5)).Returns("node5");

            return mockedBuilder;
        }

        private Graph PrepareGraph()
        {
            Graph graph = new Graph();

            GraphNode node0 = graph.AddNode(0);
            GraphNode node1 = graph.AddNode(1);
            GraphNode node2 = graph.AddNode(2);
            GraphNode node3 = graph.AddNode(3);
            GraphNode node4 = graph.AddNode(4);
            GraphNode node5 = graph.AddNode(5);

            node0.AddNeighbour(node1);
            node0.AddNeighbour(node2);

            node1.AddNeighbour(node3);
            node1.AddNeighbour(node2);

            node2.AddNeighbour(node2);
            node2.AddNeighbour(node4);

            node3.AddNeighbour(node4);
            node3.AddNeighbour(node5);

            node4.AddNeighbour(node5);

            return graph;
        }
    }
}
