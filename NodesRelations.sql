﻿CREATE TABLE [dbo].[NodesRelations] (
    [RelationId]     INT           IDENTITY (1, 1) NOT NULL,
    [NodeId]         NVARCHAR (50) NOT NULL,
    [AdjacentNodeId] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_NodeRelations] PRIMARY KEY CLUSTERED ([RelationId] ASC),
    CONSTRAINT [FK_NodeRelations_Nodes] FOREIGN KEY ([NodeId]) REFERENCES [dbo].[Nodes] ([NodeId]),
    CONSTRAINT [FK_NodeRelations_AdjacentNodes] FOREIGN KEY ([AdjacentNodeId]) REFERENCES [dbo].[Nodes] ([NodeId])
);

