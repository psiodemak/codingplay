﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfGraphServiceHostInConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            GraphService.WcfGraphContractClient service = new GraphService.WcfGraphContractClient();
            string callResult = service.TestGraphService();
            Console.WriteLine("Result: " + callResult);

            var result = service.CalculateShortestPath("node0", "node5");
            if(result != null)
            {
                Console.WriteLine(Environment.NewLine + "Path:");
                foreach(var node in result)
                {
                    Console.WriteLine(node);
                }
            }

            Console.WriteLine(Environment.NewLine + "All nodes:");
            var allNodes = service.GetAllNodes();
            if (allNodes != null)
            {
                foreach (var node in allNodes)
                {
                    Console.WriteLine(node.NodeId + ", " +node.Label);
                }
            }

            Console.WriteLine(Environment.NewLine + "All relations:");
            var allRelations = service.GetAllRelations();
            if (allRelations != null)
            {
                foreach (var relation in allRelations)
                {
                    Console.WriteLine(relation.NodeId + ", " + relation.AdjacentNodeId);
                }
            }

            Console.WriteLine(Environment.NewLine + "Press ENTER to finish....");
            Console.ReadLine();
        }
    }
}
