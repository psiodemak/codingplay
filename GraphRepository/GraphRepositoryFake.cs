﻿using CodingPlayDomain;
using GraphShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphRepository
{
    /// <summary>
    /// Fake repository for tests. Can be used instead of EF repository. 
    /// Use Bootstrap method in service to reconfigure it.
    /// </summary>
    public class GraphRepositoryFake : IGraphRepository
    {
        public List<NodeEntity> GetAllNodes()
        {
            return new List<NodeEntity>();
        }

        public List<NodeRelationEntity> GetAllRelations()
        {
            NodeRelationEntity relation01 = new NodeRelationEntity() { NodeId = "node0", AdjacentNodeId = "node1" };
            NodeRelationEntity relation02 = new NodeRelationEntity() { NodeId = "node0", AdjacentNodeId = "node2" };

            NodeRelationEntity relation12 = new NodeRelationEntity() { NodeId = "node1", AdjacentNodeId = "node2" };
            NodeRelationEntity relation13 = new NodeRelationEntity() { NodeId = "node1", AdjacentNodeId = "node3" };

            NodeRelationEntity relation22 = new NodeRelationEntity() { NodeId = "node2", AdjacentNodeId = "node2" };
            NodeRelationEntity relation24 = new NodeRelationEntity() { NodeId = "node2", AdjacentNodeId = "node4" };

            NodeRelationEntity relation34 = new NodeRelationEntity() { NodeId = "node3", AdjacentNodeId = "node4" };
            NodeRelationEntity relation35 = new NodeRelationEntity() { NodeId = "node3", AdjacentNodeId = "node5" };

            NodeRelationEntity relation45 = new NodeRelationEntity() { NodeId = "node4", AdjacentNodeId = "node5" };

            List<NodeRelationEntity> relations = new List<NodeRelationEntity>();
            relations.Add(relation01);
            relations.Add(relation02);

            relations.Add(relation12);
            relations.Add(relation13);

            relations.Add(relation22);
            relations.Add(relation24);

            relations.Add(relation34);
            relations.Add(relation35);

            relations.Add(relation45);

            return relations;
        }
    }
}
