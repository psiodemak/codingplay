﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WcfDataLoaderServiceHostInConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoaderService.WcfDataLoaderContractClient service = new DataLoaderService.WcfDataLoaderContractClient();
            string callResult = service.TestDataLoaderService();
            Console.WriteLine("Result: " + callResult);

            XElement xTest = new XElement("root");
            service.StoreXmlDataInDb(xTest);

            Console.WriteLine(Environment.NewLine + "Press ENTER to finish....");
            Console.ReadLine();
        }
    }
}
