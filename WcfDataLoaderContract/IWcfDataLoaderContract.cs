﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WcfDataLoaderContract
{
    /// <summary>
    /// Contract for data loader.
    /// </summary>
    [ServiceContract]
    public interface IWcfDataLoaderContract
    {
        [OperationContract]
        string TestDataLoaderService();

        [OperationContract]
        void StoreXmlDataInDb(XElement xmlData);
    }
}
