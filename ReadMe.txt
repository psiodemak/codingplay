1. Create SQL DB "CodingPlay" and execute scripts: Nodes.sql and NodesRelations.sql
2. Modify connection to db in .config files.

Current db is:
data source=(localdb)\MSSQLLocalDB;initial catalog=CodingPlay

Current connection string:

<connectionStrings>
    <add name="CodingPlayEntities" connectionString="metadata=res://*/CodingPlayModel.csdl|res://*/CodingPlayModel.ssdl|res://*/CodingPlayModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=(localdb)\MSSQLLocalDB;initial catalog=CodingPlay;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
</connectionStrings>

ATTENTION:
Update connection string in ALL config files where it exists. Entity Framework really needs it....

================================== DATA LOADER ==========================================

3. Create folder c:\CodingPlayData and copy xml files from "CodingPlayData". It contains 6 xml files.
   Source folder with xml files can be reconfigured by entry from .config file:
   <add key="DirectoryWithXmlFiles" value="c:\CodingPlayData"/>

4. Run WcfDataLoaderServiceHostInConsole.exe.	
   Wcf works on address: net.tcp://localhost:8734/WcfDataLoaderService
   It can be reconfigured in .config file.

5. Run "DataLoaderConsole.exe". Data should be stored in db.

================================== GUI and GRAPH WCF ==========================================

6. Run WcfGraphServiceHostInConsole.exe
   Wcf works on address: net.tcp://localhost:8732/WcfGraphService
   It can be reconfigured in .config file.

7. Run "CodingPlayGUI"
   Double click to select node. Double click to unselect node.
   Click "Calculate" to show trail between nodes.

================================== SOLUTION DESCRIPTION ==========================================

Data Loader client: 
* DataLoader - preparation of data based on xml files
* DataLoaderConsole - processing xml files and storing data into db via wcf

Data loader WCF:
* WcfDataLoaderContract - contract used by DataLoaderConsole
* WcfDataLoaderService - service for contract WcfDataLoaderContract
* WcfDataLoaderServiceHostInConsole - host of service
* WcfDataLoaderShared - additional interfaces and types used in DI

Data loader client and wcf tests:
* DataLoaderTests
* WcfDataLoaderServiceHostInConsoleTest
* WcfDataLoaderServiceTests

Graph core:
* GraphBuilder - builes graph based on repository data
* GraphProcessor - looking for trail
* GraphShared - additional interfaces and types used in DI

Graph WCF:
* WcfGraphContract - contract used by gui application
* WcfGraphService - implementation of graph contract
* WcfGraphServiceHostInConsole - host of service
* WcfGraphServiceShared - additional interfaces and types used in DI

GUI application:
* CodingPlayGUI

Small domain types and interfaces of repositories. Used across in many places.
* CodingPlayDomain

Entity Framework implementation of main repositories
* CodingPlayRepository

================================== REMARKS ==========================================

WCF console hosts shows small info when method is invoked.
GUI shows small info in "Output" panel.
Full logging is not implemented.
