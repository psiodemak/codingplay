﻿using DataLoader;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataLoaderTests
{
    [TestFixture]
    public class LoaderTests
    {
        [Test]
        public void MergeXml_Test()
        {
            string xml1 = @"<node>
                                <id>node0</id>
                                <label>Node 0</label>
                                <adjacentNodes>
                                    <id>node1</id>
                                </adjacentNodes>
                            </node>";

            string xml2 = @"<node>
                                <id>node1</id>
                                <label>Node 1</label>
                                <adjacentNodes>
                                    <id>node0</id>
                                </adjacentNodes>
                            </node>";

            XElement xelem1 = XElement.Parse(xml1);
            XElement xelem2 = XElement.Parse(xml2);

            Loader loader = new Loader();
            var mergedXml = loader.MergeXmlData(new List<XElement> { xelem1, xelem2 });
            Assert.IsNotNull(mergedXml);

            //Assert.IsTrue(mergedXml.Descendants("root").Any());
            Assert.IsTrue(mergedXml.Name == "root");
            Assert.IsTrue(mergedXml.Attributes("created").Any());

            var check = (from element in mergedXml.Descendants("node") select element).ToList();
            Assert.IsNotNull(check);
            Assert.IsTrue(check.Count == 2);

            var element0 = check[0].Element("id");
            Assert.IsTrue(element0.Value == "node0");

            var element1 = check[1].Element("id");
            Assert.IsTrue(element1.Value == "node1");
        }
    }
}
