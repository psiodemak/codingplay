﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfGraphServiceShared
{
    /// <summary>
    /// Interface to get relations.
    /// </summary>
    public interface IGetAllRelationsQuery
    {
        List<NodeRelationEntity> GetAllRelations();
    }
}
