﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfGraphServiceShared
{
    /// <summary>
    /// Interface to calculate path.
    /// </summary>
    public interface ICalculateShortestPathQuery 
    {
        List<string> CalculateShortestPath(string nodeIdFrom, string nodeIdTo);
    }
}
