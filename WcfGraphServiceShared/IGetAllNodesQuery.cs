﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfGraphServiceShared
{
    /// <summary>
    /// Interface to get nodes.
    /// </summary>
    public interface IGetAllNodesQuery
    {
        List<NodeEntity> GetAllNodes();
    }
}
