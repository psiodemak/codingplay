﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfDataLoaderServiceHostInConsole
{
    /// <summary>
    /// Host of WCF
    /// </summary>
    class Program
    {
        private static ServiceHost _host;

        /// <summary>
        /// Main method. Host is created based on .config file.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            StartService();

            Console.WriteLine("Wcf Data Loader Service is running. Press any key to finish....");
            Console.ReadKey();

            CloseService();
        }

        private static void StartService()
        {
            _host = new ServiceHost(typeof(WcfDataLoaderService.WcfDataLoaderService));
            _host.Open();
        }

        private static void CloseService()
        {
            if (_host.State != CommunicationState.Closed)
            {
                _host.Close();
            }
        }
    }
}
