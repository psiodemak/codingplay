﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayGUI.Model
{
    public class Graph
    {
        public ICollection<Node> Nodes { get; set; }
        public ICollection<Relation> Relations { get; set; }
    }
}
