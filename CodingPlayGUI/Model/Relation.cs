﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayGUI.Model
{
    public class Relation
    {
        public string Node1 { get; set; }
        public string Node2 { get; set; }

        public Relation(string node1, string node2)
        {
            Node1 = node1;
            Node2 = node2;
        }
    }
}
