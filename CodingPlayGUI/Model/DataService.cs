﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace CodingPlayGUI.Model
{
    public class DataService : IDataService, IDisposable
    {
        private GraphService.WcfGraphContractClient _service;

        public Graph GetGraph()
        {
            Graph retVal = new Graph();

            if (_service == null)
            {
                _service = new GraphService.WcfGraphContractClient();
            }

            var allNodes = _service.GetAllNodes();
            var allRelations = _service.GetAllRelations();

            List<Node> nodesUi = (from nodeCore in allNodes select new Node(nodeCore.NodeId, nodeCore.Label)).ToList();
            List<Relation> relationsUi = (from relationCore in allRelations select new Relation(relationCore.NodeId, relationCore.AdjacentNodeId)).ToList();

            retVal.Nodes = nodesUi;
            retVal.Relations = relationsUi;

            return retVal;
        }

        public string[] CalculateShortestPath(string from, string to)
        {
            string[] retVal = new string[0];
            retVal = _service.CalculateShortestPath(from, to);
            return retVal;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_service != null)
                    {
                        try
                        {
                            if (_service.State != CommunicationState.Faulted)
                            {
                                _service.Close();
                            }
                        }
                        finally
                        {
                            if (_service.State != CommunicationState.Closed)
                            {
                                _service.Abort();
                            }
                        }
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finalizer.
        /// </summary>
        ~DataService()
        {
            Dispose(false);
        }
    }
}