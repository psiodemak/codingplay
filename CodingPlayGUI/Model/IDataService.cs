﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodingPlayGUI.Model
{
    public interface IDataService
    {
        Graph GetGraph();
        string[] CalculateShortestPath(string from, string to);
    }
}
