﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayGUI.Model
{
    public class Node
    {
        public Node(string id, string label)
        {
            NodeId = id;
            Label = label;
        }

        public string NodeId { get;  private set; }
        public string Label { get; private set; }
    }
}
