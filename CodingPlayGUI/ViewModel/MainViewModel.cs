﻿using GalaSoft.MvvmLight;
using CodingPlayGUI.Model;
using System.Collections.Generic;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using System.Linq;
using System.ComponentModel;
using CodingPlayGUI.Command;
using System.Text;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodingPlayGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;
        private readonly ICalculateShortestPathCommand _calculatePathCommand;

        private Dictionary<string, NodeViewModel> _nodesDict = new Dictionary<string, NodeViewModel>();
        private ICollection<NodeRelationViewModel> _nodesRelations;

        private IEnumerable<string> _startEnd;

        private RelayCommand _calcShortestPathCommand;

        private StringBuilder _applicationOutput = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService, ICalculateShortestPathCommand calculatePathCommand)
        {
            _dataService = dataService;
            _calculatePathCommand = calculatePathCommand;

            _applicationOutput.Append("Preparing graph data...");
            try
            {
                GetGraphUi(500);
            }
            catch(Exception ex)
            {
                _applicationOutput.Append(ex.ToString());
            }
            finally
            {
                _applicationOutput.Append(Environment.NewLine + "Done");
            }            
        }

        public string Output
        {
            get
            {
                return _applicationOutput.ToString();
            }

            set
            {
                _applicationOutput.Append(Environment.NewLine);
                _applicationOutput.Append(value);

                RaisePropertyChanged();
            }
        }

        public IEnumerable<NodeViewModel> Nodes
        {
            get { return _nodesDict.Values; }
        }

        public IEnumerable<NodeRelationViewModel> NodesRelations
        {
            get { return _nodesRelations; }
        }
        
        public ICommand CalcShortestPathCommand
        {
            get
            {
                if (_calcShortestPathCommand == null)
                {
                    _calcShortestPathCommand = new RelayCommand(() => CalculateShorestPathCallerWithAsync(), () => CanCalcShortestPath());
                }

                return _calcShortestPathCommand;
            }
        }

        private void GetGraphUi(object width)
        {
            var graph = _dataService.GetGraph();
            _nodesDict = GraphHelper.BuildNodes(graph, Vm_PropertyChanged);
            _nodesRelations = GraphHelper.BuildRelations(graph, _nodesDict);

            _startEnd = (from node in _nodesDict.Values where node.IsSelected select node.NodeId);
        }

        private void Vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsSelected"))
            {
                _startEnd = (from node in _nodesDict.Values where node.IsSelected select node.NodeId);

                foreach (var n in _nodesDict.Values)
                {
                    n.CanSelect = n.IsSelected || _startEnd.Count() != 2;
                    n.IsOnShortestPath = false;
                }
            }
        }

        private bool _calculationInProgress = false;
        private bool CanCalcShortestPath()
        {
            return !_calculationInProgress && _calculatePathCommand.CanCalculate(_startEnd);
        }

        private Task CalcShortestPathAsync()
        {
            return Task.Run(() =>
            {
                CalcShortestPath();
            });
        }

        private async void CalculateShorestPathCallerWithAsync()
        {
            await CalcShortestPathAsync();            
        }

        private void CalcShortestPath()
        {
            Output = "Calculate shortest path...";
            try
            {
                _calculationInProgress = true;
                _calculatePathCommand.Calculate(_startEnd, _nodesDict);
            }
            catch (Exception ex)
            {
                Output = ex.ToString();
            }
            finally
            {
                _calculationInProgress = false;
                Output = "Done";
            }
        }
    }
}