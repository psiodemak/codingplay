﻿using GalaSoft.MvvmLight;
using System;
using System.Windows;

namespace CodingPlayGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class NodeViewModel : ViewModelBase, IEquatable<NodeViewModel>
    {
        private double _x = 0;
        private double _y = 0;
        private Point _connectorHotspot;
        private string _label;
        private bool _isSelected = false;
        private bool _isOnShortestPath = false;
        private bool _canSelect = true;

        /// <summary>
        /// Initializes a new instance of the NodeViewModel class.
        /// </summary>
        public NodeViewModel(double x, double y, string label, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("Id is required");
            }

            _x = x;
            _y = y;
            _label = label;
            NodeId = id;
        }

        #region Prop

        public string NodeId
        {
            get; private set;
        }

        public double X
        {
            get { return _x; }
            set
            {
                if (_x != value)
                {
                    _x = value;
                    RaisePropertyChanged();
                }
            }
        }

        public double Y
        {
            get { return _y; }
            set
            {
                if (_y != value)
                {
                    _y = value;
                    RaisePropertyChanged();
                }
            }
        }

        public string Label
        {
            get { return _label; }
            set
            {
                if (_label != value)
                {
                    _label = value;
                    RaisePropertyChanged();
                }
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_canSelect && _isSelected != value)
                {
                    _isSelected = value;
                    RaisePropertyChanged();
                }
            }
        }

        public bool IsOnShortestPath
        {
            get { return _isOnShortestPath; }
            set
            {
                if (_isOnShortestPath != value)
                {
                    _isOnShortestPath = value;
                    RaisePropertyChanged();
                }
            }
        }

        public bool CanSelect
        {
            get { return _canSelect; }
            set
            {
                if (_canSelect != value)
                {
                    _canSelect = value;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// The hotspot of the connector.
        /// This value is pushed through from the UI because it is data-bound to 'Hotspot'
        /// in ConnectorItem.
        /// </summary>
        public Point ConnectorHotspot
        {
            get { return _connectorHotspot; }
            set
            {
                if (_connectorHotspot != value)
                {
                    _connectorHotspot = value;
                    RaisePropertyChanged();
                }
            }
        }

        #endregion Prop

        public bool Equals(NodeViewModel other)
        {
            if (other == null)
            {
                return false;
            }

            if (other == this)
            {
                return true;
            }

            return NodeId.Equals(other.NodeId, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj == this)
            {
                return true;
            }

            if (obj is NodeRelationViewModel)
            {
                return Equals(obj as NodeRelationViewModel);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return NodeId.GetHashCode();
        }

    }
}