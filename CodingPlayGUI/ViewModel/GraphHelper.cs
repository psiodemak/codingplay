﻿using CodingPlayGUI.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayGUI.ViewModel
{
    public class GraphHelper
    {
        private const double MAXX = 400;

        public static Dictionary<string, NodeViewModel> BuildNodes(Graph graph, PropertyChangedEventHandler eventHandler)
        {
            var nodesDict = new Dictionary<string, NodeViewModel>();

            int x = -90;
            int y = 10;
            int yShift = 0;

            foreach (var node in graph.Nodes)
            {
                x += 100;
                int tempY = y + yShift;

                if (yShift == 50)
                {
                    yShift = 0;
                }
                else
                {
                    yShift = 50;
                }

                if (x >= MAXX)
                {
                    y += 150;
                    tempY = y;
                    x = 30;
                }

                var vm = new NodeViewModel(x, tempY, node.Label, node.NodeId);
                nodesDict.Add(vm.NodeId, vm);
                vm.PropertyChanged += eventHandler;
            }

            return nodesDict;
        }

        public static List<NodeRelationViewModel> BuildRelations(Graph graph, Dictionary<string, NodeViewModel> nodesDict)
        {
            var nodesRelations = new List<NodeRelationViewModel>();

            foreach (var rel in graph.Relations)
            {
                NodeViewModel nodeFrom;
                NodeViewModel nodeTo;

                if (nodesDict.TryGetValue(rel.Node1, out nodeFrom) && nodesDict.TryGetValue(rel.Node2, out nodeTo))
                {
                    nodesRelations.Add(new NodeRelationViewModel(nodeFrom, nodeTo));
                }
            }

            return nodesRelations;
        }
    }
}
