﻿using GalaSoft.MvvmLight;
using System;

namespace CodingPlayGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class NodeRelationViewModel : ViewModelBase
    {
        private NodeViewModel _node1 = null;
        private NodeViewModel _node2 = null;

        /// <summary>
        /// Initializes a new instance of the NodeRelationViewModel class.
        /// </summary>
        public NodeRelationViewModel(NodeViewModel node1, NodeViewModel node2)
        {
            if (node1 == null || node2 == null)
            {
                throw new ArgumentException("Nodes cannot be null");
            }

            _node1 = node1;
            _node2 = node2;
        }

        public NodeViewModel Node1
        {
            get { return _node1; }
            set
            {
                _node1 = value;
                RaisePropertyChanged();
            }
        }

        public NodeViewModel Node2
        {
            get { return _node2; }
            set
            {
                _node2 = value;
                RaisePropertyChanged();
            }
        }

        public bool IsLoop
        {
            get { return _node1.Equals(_node2); }
        }
    }
}