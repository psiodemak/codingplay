﻿using CodingPlayGUI.Model;
using CodingPlayGUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayGUI.Command
{
    public class CalculateShortestPathCommand : ICalculateShortestPathCommand
    {
        private readonly IDataService _dataService;

        public CalculateShortestPathCommand(IDataService dataService)
        {
            _dataService = dataService;
        }

        public void Calculate(IEnumerable<string> startEnd, Dictionary<string, NodeViewModel> nodesDict)
        {
            string from = startEnd.First();
            string to = startEnd.Last();

            var result = _dataService.CalculateShortestPath(from, to);

            foreach (var nodeId in result)
            {
                NodeViewModel node;
                if (nodesDict.TryGetValue(nodeId, out node))
                {
                    node.IsOnShortestPath = true;
                }
            }
        }

        public bool CanCalculate(IEnumerable<string> startEnd)
        {
            return startEnd != null && startEnd.Count() == 2;
        }
    }
}
