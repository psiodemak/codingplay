﻿using CodingPlayGUI.Model;
using CodingPlayGUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayGUI.Command
{
    public interface ICalculateShortestPathCommand
    {
        void Calculate(IEnumerable<string> startEnd, Dictionary<string, NodeViewModel> nodesDict);
        bool CanCalculate(IEnumerable<string> startEnd);
    }
}
