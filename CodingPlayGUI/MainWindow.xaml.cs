﻿using System.Windows;
using CodingPlayGUI.ViewModel;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace CodingPlayGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        }

        #region Event Handlers

        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Thumb thumb = (Thumb)sender;
            NodeViewModel node = (NodeViewModel)thumb.DataContext;
            var newX = node.X + e.HorizontalChange;
            var newY = node.Y + e.VerticalChange;
            node.X = newX < 0 ? 0 : newX;
            node.Y = newY < 0 ? 0 : newY;

        }

        private void Thumb_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Thumb thumb = (Thumb)sender;
            if (!thumb.IsDragging)
            {
                NodeViewModel node = (NodeViewModel)thumb.DataContext;
                node.IsSelected = !node.IsSelected;
            }
        }

        #endregion Event Handlers
    }
}