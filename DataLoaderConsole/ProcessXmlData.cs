﻿using DataLoader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataLoaderConsole
{
    /// <summary>
    /// Processing xml data.
    /// </summary>
    public class ProcessXmlData : IDisposable
    {
        private DataLoaderService.WcfDataLoaderContractClient _service;

        /// <summary>
        /// Processing xml data. 
        /// 1. Read data from files
        /// 2. Produce xml with data for WCF
        /// 3. Execute WCf method to store data in DB.
        /// </summary>
        /// <param name="path"></param>
        public void Process(string path)
        {
            List<XElement> elementsToMerge = CollectXmlDataFromFiles(path);
            if (elementsToMerge.Count > 0)
            {
                Console.WriteLine("Merging xml files");
                Loader loader = new Loader();
                XElement fullXml = loader.MergeXmlData(elementsToMerge);

                Console.WriteLine("Sending data to WCF");
                _service = new DataLoaderService.WcfDataLoaderContractClient();
                _service.StoreXmlDataInDb(fullXml);
            }
        }

        private List<XElement> CollectXmlDataFromFiles(string path)
        {
            List<XElement> elementsToMerge = new List<XElement>();

            if (Directory.Exists(path))
            {
                string[] xmlFiles = Directory.GetFiles(path, "*.xml");                
                foreach (string xmlFile in xmlFiles)
                {
                    try
                    {
                        Console.WriteLine("Loading file: " + xmlFile);
                        XElement xml = XElement.Load(xmlFile);
                        elementsToMerge.Add(xml);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }

            return elementsToMerge;
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Dispose worker method. Handles graceful shutdown of the
        /// client even if it is an faulted state.
        /// </summary>
        /// <param name="disposing">Are we disposing (alternative
        /// is to be finalizing)</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_service != null)
                {
                    try
                    {
                        if (_service.State != CommunicationState.Faulted)
                        {
                            _service.Close();
                        }
                    }
                    finally
                    {
                        if (_service.State != CommunicationState.Closed)
                        {
                            _service.Abort();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Finalizer.
        /// </summary>
        ~ProcessXmlData()
        {
            Dispose(false);
        }
    }
}
