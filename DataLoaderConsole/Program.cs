﻿using DataLoader;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataLoaderConsole
{
    class Program
    {
        private const string DEFAULT_DIR = @"c:\CodingPlayData";

        /// <summary>
        /// Main method. 
        /// 1. Prepare path to analyze
        /// 2. Execute processing.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string path = GetDirectoryWithXmlFiles();

            using (ProcessXmlData processor = new ProcessXmlData())
            {
                processor.Process(path);
            }

            Console.WriteLine("Press ENTER to finish....");
            Console.ReadLine();
        }

        private static string GetDirectoryWithXmlFiles()
        {
            string retVal = DEFAULT_DIR;

            string pathFromConfigFile = ConfigurationManager.AppSettings["DirectoryWithXmlFiles"];
            if(!string.IsNullOrWhiteSpace(pathFromConfigFile))
            {
                retVal = pathFromConfigFile;
            }

            return pathFromConfigFile;
        }
    }
}
