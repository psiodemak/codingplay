﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayDomain
{
    /// <summary>
    /// Repository interface used in data loader wcf. It allows read write operations on repository.
    /// </summary>
    public interface IDataLoaderReadWriteRepository
    {
        void AddNode(string nodeId, string label);
        void AddRelation(string nodeId, string adjacentNodeId);
        void Save();
        void DeleteAllData();
    }
}
