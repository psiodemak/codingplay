﻿using CodingPlayDomain;
using System.Collections.Generic;

namespace CodingPlayDomain
{
    /// <summary>
    /// Interface used in graph repository wcf. Allows on read only access to repository.
    /// </summary>
    public interface IGraphRepository
    {
        List<NodeEntity> GetAllNodes();

        List<NodeRelationEntity> GetAllRelations();
    }
}
