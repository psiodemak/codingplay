﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayDomain
{
    /// <summary>
    /// Node relation entity. Used also as DTO between wcf and gui client.
    /// </summary>
    public class NodeRelationEntity
    {
        public string NodeId { get; set; }
        public string AdjacentNodeId { get; set; }
    }
}
