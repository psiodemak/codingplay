﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPlayDomain
{
    /// <summary>
    /// Node entity. Used also as DTO between wcf and GUI client.
    /// </summary>
    public class NodeEntity
    {
        public string NodeId { get; set; }
        public string Label { get; set;}
    }
}
