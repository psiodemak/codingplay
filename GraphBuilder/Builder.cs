﻿using CodingPlayDomain;
using GraphShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphBuilder
{   
    /// <summary>
    /// Builds graph from repository data.
    /// </summary>
    public class Builder : IGraphBuilder
    {
        private IGraphRepository _repository;

        private Dictionary<string, GraphNode> _mapNodeIdToGraphNode;
        private Dictionary<int, string> _mapGraphIdToNodeId;

        public Builder(IGraphRepository repository)
        {
            _repository = repository;
            _mapNodeIdToGraphNode = new Dictionary<string, GraphNode>();
            _mapGraphIdToNodeId = new Dictionary<int, string>();
        }

        /// <summary>
        /// Prepare graph. Relations from repository data are used.
        /// Graph is based on integer numbers.
        /// Mapping to db ids is provided.
        /// </summary>
        /// <returns>
        /// Graph instance
        /// </returns>
        public Graph PrepareGraph()
        {
            Graph graph = new Graph();

            _mapNodeIdToGraphNode.Clear();
            _mapGraphIdToNodeId.Clear();

            int nodesCounter = 0;

            var allRelations = _repository.GetAllRelations();
            if(allRelations != null)
            {
                foreach(var relation in allRelations)
                {
                    GraphNode node = null;
                    if(!_mapNodeIdToGraphNode.TryGetValue(relation.NodeId, out node))
                    {
                        node = graph.AddNode(nodesCounter);
                        _mapNodeIdToGraphNode.Add(relation.NodeId, node);
                        _mapGraphIdToNodeId.Add(node.Id, relation.NodeId);
                        nodesCounter++;
                    }

                    GraphNode adjacentNode = null;
                    if (!_mapNodeIdToGraphNode.TryGetValue(relation.AdjacentNodeId, out adjacentNode))
                    {
                        adjacentNode = graph.AddNode(nodesCounter);
                        _mapNodeIdToGraphNode.Add(relation.AdjacentNodeId, adjacentNode);
                        _mapGraphIdToNodeId.Add(adjacentNode.Id, relation.AdjacentNodeId);
                        nodesCounter++;
                    }

                    node.AddNeighbour(adjacentNode);
                }
            }

            return graph;
        }

        /// <summary>
        /// Node Id to integer number. Graph is based on integers.
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public int GetGraphIdForNodeId(string nodeId)
        {
            GraphNode node = null;
            if(nodeId != null && _mapNodeIdToGraphNode.TryGetValue(nodeId, out node))
            {
                return node.Id;
            }

            return -1;
        }

        /// <summary>
        /// Graph id to node id.
        /// </summary>
        /// <param name="graphId"></param>
        /// <returns></returns>
        public string GetNodeIdForGraphId(int graphId)
        {
            string nodeId;
            if(_mapGraphIdToNodeId.TryGetValue(graphId, out nodeId))
            {
                return nodeId;
            }

            return string.Empty;
        }
    }
}
