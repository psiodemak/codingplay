﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataLoader
{
    /// <summary>
    /// Class responsible for preparation of data send to wcf service.
    /// </summary>
    public class Loader
    {
        /// <summary>
        /// Creates one xml document with all data readed from files. Added element "root" and creation date.
        /// </summary>
        /// <param name="xmlData">
        /// Xml data readed from files.
        /// </param>
        /// <returns></returns>
        public XElement MergeXmlData(List<XElement> xmlData)
        {
            XElement xRoot = new XElement("root");
            XAttribute created = new XAttribute("created", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            xRoot.Add(created);

            xRoot.Add(xmlData);

            return xRoot;
        }
    }
}
