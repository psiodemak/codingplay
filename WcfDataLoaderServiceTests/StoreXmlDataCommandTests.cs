﻿using CodingPlayDomain;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WcfDataLoaderService;
using WcfDataLoaderShared;

namespace WcfDataLoaderServiceTests
{
    [TestFixture]
    public class StoreXmlDataCommandTests
    {
        [Test]
        public void StoreXmlDataCommand_Test()
        {
            IDataLoaderReadWriteRepository repository = A.Fake<IDataLoaderReadWriteRepository>();

            ParseXmlResult parseResult = new ParseXmlResult();
            parseResult.Nodes.Add(new NodeEntity { NodeId = "node0", Label = "label" });
            parseResult.Nodes.Add(new NodeEntity { NodeId = "node1", Label = "label" });
            parseResult.Relations.Add(new NodeRelationEntity { NodeId = "node0", AdjacentNodeId = "node1" });

            IParseXml xmlParser = A.Fake<IParseXml>();
            A.CallTo(() => xmlParser.Execute(A<XElement>.Ignored)).Returns(parseResult);

            StoreXmlDataCommand command = new StoreXmlDataCommand(repository, xmlParser);
            command.StoreXmlData(null);

            A.CallTo(() => xmlParser.Execute(A<XElement>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => repository.DeleteAllData()).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => repository.AddNode(A<string>.Ignored, A<string>.Ignored)).MustHaveHappened(Repeated.Exactly.Twice);
            A.CallTo(() => repository.AddRelation(A<string>.Ignored, A<string>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => repository.Save()).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}
