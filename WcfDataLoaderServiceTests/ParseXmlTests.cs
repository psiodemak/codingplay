﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WcfDataLoaderService;

namespace WcfDataLoaderServiceTests
{
    [TestFixture]
    public class ParseXmlTests
    {
        [Test]
        public void Parse_Test()
        {
            string xml = @"
<root created=""2016-09-04 21:23:40"">
  <node>
    <id>node0</id>
    <label>Node 0</label>
    <adjacentNodes>
      <id>node1</id>
    </adjacentNodes>
  </node>
  <node>
    <id>node1</id>
    <label>Node 1</label>
    <adjacentNodes>
      <id>node0</id>
    </adjacentNodes>
  </node>
</root>
";
            XElement xRoot = XElement.Parse(xml);

            ParseXml parser = new ParseXml();
            var result = parser.Execute(xRoot);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Nodes);
            Assert.IsNotNull(result.Relations);
            Assert.IsTrue(result.Nodes.Count == 2);
            Assert.IsTrue(result.Relations.Count == 2);
        }

        [Test]
        public void Parse_RelationToNotExistingNode_Test()
        {
            string xml = @"
<root created=""2016-09-04 21:23:40"">
  <node>
    <id>node0</id>
    <label>Node 0</label>
    <adjacentNodes>
      <id>node1</id>
    </adjacentNodes>
  </node>
</root>
";
            XElement xRoot = XElement.Parse(xml);

            ParseXml parser = new ParseXml();
            var result = parser.Execute(xRoot);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Nodes);
            Assert.IsNotNull(result.Relations);
            Assert.IsTrue(result.Nodes.Count == 1);
            Assert.IsTrue(result.Relations.Count == 0);
        }

        [Test]
        public void Parse_6ElementsGraph_Test()
        {
            string xml = @"
<root created=""2016-09-04 23:02:33"">
  <node>
    <id>node0</id>
    <label>Node 0</label>
    <adjacentNodes>
      <id>node1</id>
      <id>node2</id>
    </adjacentNodes>
  </node>
  <node>
    <id>node1</id>
    <label>Node 1</label>
    <adjacentNodes>
      <id>node0</id>
      <id>node2</id>
      <id>node3</id>
    </adjacentNodes>
  </node>
  <node>
    <id>node2</id>
    <label>Node 2</label>
    <adjacentNodes>
      <id>node0</id>
      <id>node1</id>
      <id>node2</id>
      <id>node4</id>
    </adjacentNodes>
  </node>
  <node>
    <id>node3</id>
    <label>Node 3</label>
    <adjacentNodes>
      <id>node1</id>
      <id>node4</id>
      <id>node5</id>
    </adjacentNodes>
  </node>
  <node>
    <id>node4</id>
    <label>Node 4</label>
    <adjacentNodes>
      <id>node2</id>
      <id>node3</id>
      <id>node5</id>
    </adjacentNodes>
  </node>
  <node>
    <id>node5</id>
    <label>Node 5</label>
    <adjacentNodes>
      <id>node3</id>
      <id>node4</id>
    </adjacentNodes>
  </node>
</root>

";

            XElement xRoot = XElement.Parse(xml);

            ParseXml parser = new ParseXml();
            var result = parser.Execute(xRoot);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Nodes);
            Assert.IsNotNull(result.Relations);
            Assert.IsTrue(result.Nodes.Count == 6);
            Assert.IsTrue(result.Relations.Count == 17);
        }

    }
}
