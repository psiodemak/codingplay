﻿using GraphShared;
using System.Collections.Generic;

namespace GraphProcessor
{
    /// <summary>
    /// Calculation of trail in graph between two nodes.
    /// </summary>
    public class FindPath : IGraphTrailFinder
    {        
        /// <summary>
        /// Analyze of graph to find shortest trail between two nodes.
        /// </summary>
        /// <param name="graph">Graph</param>
        /// <param name="fromId">Node id to start</param>
        /// <param name="toId">Node it to finish</param>
        /// <returns>
        /// Trail from one node to another. It's list of nodes.
        /// </returns>
        public List<int> FindTrail(Graph graph, int fromId, int toId)
        {
            List<int> retVal = new List<int>();

            if (graph == null || graph.Nodes == null || graph.Nodes.Count == 0)
            {
                return retVal;
            }

            if (fromId == -1 || toId == -1 || fromId == toId || fromId >= graph.Nodes.Count || toId >= graph.Nodes.Count)
            {
                return retVal;
            }

            bool[] visited = new bool[graph.Nodes.Count];
            int[] previous = new int[graph.Nodes.Count];
            int[] distance = new int[graph.Nodes.Count];

            for(int index = 0; index < graph.Nodes.Count; index++)
            {
                previous[index] = -1;
                distance[index] = -1;
            }

            GraphNode start = graph.Nodes[fromId];

            Queue<GraphNode> nodesToCheck = new Queue<GraphNode>();
            previous[start.Id] = -1;
            distance[start.Id] = 0;
            nodesToCheck.Enqueue(start);

            bool trailFound = false;
            while(nodesToCheck.Count > 0)
            {
                GraphNode node = nodesToCheck.Dequeue();
                visited[node.Id] = true;

                foreach(var neighbour in node.AdjacentNodes)
                {
                    if(visited[neighbour.Id])
                    {
                        continue;
                    }

                    if (distance[neighbour.Id] == -1)
                    {
                        previous[neighbour.Id] = node.Id;
                        distance[neighbour.Id] = distance[node.Id] + 1;
                        nodesToCheck.Enqueue(neighbour);
                    }

                    if(neighbour.Id == toId)
                    {
                        trailFound = true;
                        nodesToCheck.Clear();
                        break;
                    }                    
                }
            }

            if(trailFound)
            {
                retVal.Add(toId);
                int count = distance[toId];

                int nodeId = toId;
                for(int i = 0; i< count; i++)
                {
                    nodeId = previous[nodeId];
                    retVal.Add(nodeId);
                }
            }

            return retVal;
        }       
    }
}
