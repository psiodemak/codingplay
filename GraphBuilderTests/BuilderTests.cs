﻿using CodingPlayDomain;
using FakeItEasy;
using GraphBuilder;
using GraphShared;
using NUnit.Framework;
using System.Collections.Generic;

namespace GraphBuilderTests
{
    [TestFixture]
    public class BuilderTests
    {
        [Test]
        public void Builder_TwoNodes_Test()
        {
            NodeRelationEntity relation = new NodeRelationEntity() { NodeId = "node0", AdjacentNodeId = "node1" };
            IGraphRepository mockedRepo = A.Fake<IGraphRepository>();
            A.CallTo(() => mockedRepo.GetAllRelations()).Returns(new List<NodeRelationEntity> { relation });

            Builder builder = new Builder(mockedRepo);

            Graph graph = builder.PrepareGraph();
            Assert.IsNotNull(graph);
            Assert.IsTrue(graph.Nodes.Count == 2);
            Assert.IsTrue(graph.Nodes[0].AdjacentNodes.Count == 1);
        }

        [Test]
        public void Builder_SmallStructure_Test()
        {           
            IGraphRepository mockedRepo = PrepareSmallStructure();

            Builder builder = new Builder(mockedRepo);

            Graph graph = builder.PrepareGraph();
            Assert.IsNotNull(graph);
            Assert.IsTrue(graph.Nodes.Count == 6);
            Assert.IsTrue(graph.Nodes[0].AdjacentNodes.Count == 2); // node0
            Assert.IsTrue(graph.Nodes[2].AdjacentNodes.Count == 4); // node2
        }

        [Test]
        public void Builder_NotValidInput_Test()
        {
            IGraphRepository mockedRepo = PrepareSmallStructure();

            Builder builder = new Builder(mockedRepo);
            Graph graph = builder.PrepareGraph();

            var check = builder.GetGraphIdForNodeId(null);
            Assert.IsTrue(check == -1);

            check = builder.GetGraphIdForNodeId(string.Empty);
            Assert.IsTrue(check == -1);

            var check2 = builder.GetNodeIdForGraphId(-1);
            Assert.IsEmpty(check2);
        }

        private IGraphRepository PrepareSmallStructure()
        {
            NodeRelationEntity relation01 = new NodeRelationEntity() { NodeId = "node0", AdjacentNodeId = "node1" };
            NodeRelationEntity relation02 = new NodeRelationEntity() { NodeId = "node0", AdjacentNodeId = "node2" };

            NodeRelationEntity relation12 = new NodeRelationEntity() { NodeId = "node1", AdjacentNodeId = "node2" };
            NodeRelationEntity relation13 = new NodeRelationEntity() { NodeId = "node1", AdjacentNodeId = "node3" };

            NodeRelationEntity relation22 = new NodeRelationEntity() { NodeId = "node2", AdjacentNodeId = "node2" };
            NodeRelationEntity relation24 = new NodeRelationEntity() { NodeId = "node2", AdjacentNodeId = "node4" };

            NodeRelationEntity relation34 = new NodeRelationEntity() { NodeId = "node3", AdjacentNodeId = "node4" };
            NodeRelationEntity relation35 = new NodeRelationEntity() { NodeId = "node3", AdjacentNodeId = "node5" };

            NodeRelationEntity relation45 = new NodeRelationEntity() { NodeId = "node4", AdjacentNodeId = "node5" };

            List<NodeRelationEntity> relations = new List<NodeRelationEntity>();
            relations.Add(relation01);
            relations.Add(relation02);

            relations.Add(relation12);
            relations.Add(relation13);

            relations.Add(relation22);
            relations.Add(relation24);

            relations.Add(relation34);
            relations.Add(relation35);

            relations.Add(relation45);

            IGraphRepository mockedRepo = A.Fake<IGraphRepository>();
            A.CallTo(() => mockedRepo.GetAllRelations()).Returns(relations);

            return mockedRepo;
        }
    }
}
