﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WcfDataLoaderShared;

namespace WcfDataLoaderService
{
    /// <summary>
    /// Command responsible for storing xml data in DB.
    /// </summary>
    public class StoreXmlDataCommand : IStoreXmlDataCommand
    {
        private readonly IDataLoaderReadWriteRepository _repository;
        private readonly IParseXml _xmlParser;

        public StoreXmlDataCommand(IDataLoaderReadWriteRepository repository, IParseXml xmlParser)
        {
            _repository = repository;
            _xmlParser = xmlParser;
        }

        /// <summary>
        /// 1. Parse XML
        /// 2. Delete all existing data
        /// 3. Prepare nodes and relations in repository
        /// 4. Save data in repository
        /// </summary>
        /// <param name="xmlData">
        /// Xml data from data loader.
        /// </param>
        public void StoreXmlData(XElement xmlData)
        {
            ParseXmlResult parseResult = _xmlParser.Execute(xmlData);

            _repository.DeleteAllData();

            foreach(var node in parseResult.Nodes)
            {
                _repository.AddNode(node.NodeId, node.Label);
            }

            foreach(var relation in parseResult.Relations)
            {
                _repository.AddRelation(relation.NodeId, relation.AdjacentNodeId);
            }

            _repository.Save();
        }
    }
}
