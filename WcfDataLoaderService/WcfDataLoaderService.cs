﻿using CodingPlayDomain;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WcfDataLoaderContract;
using WcfDataLoaderShared;

namespace WcfDataLoaderService
{
    /// <summary>
    /// Service responsible for storing data into DB.
    /// </summary>
    public class WcfDataLoaderService : IWcfDataLoaderContract
    {
        private readonly Container _container;

        /// <summary>
        /// Constructor. Prepares data for dependency injection.
        /// </summary>
        public WcfDataLoaderService()
        {
            _container = Bootstrap();
        }

        public string TestDataLoaderService()
        {
            string retVal = "WcfDataLoaderService is running on machine: " + Environment.MachineName;
            Console.WriteLine(retVal);
            return retVal;
        }

        /// <summary>
        /// Storing xml data in db. Executes command for IStoreXmlDataCommand.
        /// </summary>
        /// <param name="xmlData"></param>
        public void StoreXmlDataInDb(XElement xmlData)
        {
            Console.WriteLine("WcfDataLoaderService: StoreXmlDataInDb");

            try
            {
                var command = _container.GetInstance<IStoreXmlDataCommand>();
                command.StoreXmlData(xmlData);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private Container Bootstrap()
        {
            var container = new Container();

            container.Register<IDataLoaderReadWriteRepository, CodingPlayRepository.CodingPlayRepository>();
            container.Register<IParseXml, ParseXml>();
            container.Register<IStoreXmlDataCommand, StoreXmlDataCommand>();

            container.Verify();

            return container;
        }
    }
}
