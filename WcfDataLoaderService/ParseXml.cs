﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WcfDataLoaderShared;

namespace WcfDataLoaderService
{
    /// <summary>
    /// XML parser.
    /// </summary>
    public class ParseXml : IParseXml
    {
        /// <summary>
        /// Parse xml into domain entities: NodeEntity and NodeRelationEntity.
        /// Input data is verified. Relations to not existing nodes are removed.
        /// </summary>
        /// <param name="xRoot">
        /// Root of xml document
        /// </param>
        /// <returns>
        /// Collection of nodes and relations.
        /// </returns>
        public ParseXmlResult Execute(XElement xRoot)
        {
            ParseXmlResult result = new ParseXmlResult();

            List<NodeEntity> nodes = new List<NodeEntity>();
            List<NodeRelationEntity> relations = new List<NodeRelationEntity>();

            if(xRoot != null)
            {
                HashSet<string> createdNodes = new HashSet<string>();
                HashSet<string> createdRelations = new HashSet<string>();
                HashSet<string> adjacentNodesToVerify = new HashSet<string>();

                var xmlNodes = (from element in xRoot.Descendants("node") select element).ToList();
                foreach(var xmlNode in xmlNodes)
                {
                    string nodeId = xmlNode.Element("id").Value.ToLower();
                    string label = xmlNode.Element("label").Value;

                    if(!createdNodes.Contains(nodeId))
                    {
                        createdNodes.Add(nodeId);
                        var node = new NodeEntity { NodeId = nodeId, Label = label };
                        nodes.Add(node);

                        var adjacentNodes = (from element in xmlNode.Elements("adjacentNodes").Descendants("id") select element).ToList();
                        foreach(var adjacentNode in adjacentNodes)
                        {
                            string adjacentNodeId = adjacentNode.Value.ToLower();
                            string relationKey = nodeId + "_" + adjacentNodeId;
                            if(!createdRelations.Contains(relationKey))
                            {
                                createdRelations.Add(relationKey);
                                var relation = new NodeRelationEntity { NodeId = nodeId, AdjacentNodeId = adjacentNodeId };
                                relations.Add(relation);
                                adjacentNodesToVerify.Add(adjacentNodeId);
                            }
                        }
                    }
                }

                VerifyIfAdjacentNodesExists(createdNodes, adjacentNodesToVerify, relations);

                result.Nodes.AddRange(nodes);
                result.Relations.AddRange(relations);
            }

            return result;
        }

        private void VerifyIfAdjacentNodesExists(HashSet<string> createdNodes, HashSet<string> adjacentNodesToVerify, List<NodeRelationEntity> relations)
        {
            adjacentNodesToVerify.ExceptWith(createdNodes);
            if (adjacentNodesToVerify.Count > 0)
            {
                relations.RemoveAll((s) => adjacentNodesToVerify.Contains(s.AdjacentNodeId));
            }
        }
    }
}
