﻿using GraphBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphContract;
using SimpleInjector;
using GraphShared;
using GraphRepository;
using CodingPlayDomain;
using WcfGraphServiceShared;
using GraphProcessor;

namespace WcfGraphService
{
    /// <summary>
    /// Service reponsible for graph operations. Used by GUI client to clreate graph and calculate path.
    /// </summary>
    public class WcfGraphService : IWcfGraphContract
    {
        private readonly Container _container;

        /// <summary>
        /// Constructor. Bootstrap prepares data for dependency injection.
        /// </summary>
        public WcfGraphService()
        {
            _container = Bootstrap();
        }

        public string TestGraphService()
        {
            string retVal = "WcfGraphService from " + Environment.MachineName;
            Console.WriteLine(retVal);
            return retVal;
        }

        /// <summary>
        /// Call command to calculate path.
        /// </summary>
        /// <param name="nodeIdFrom">From node id</param>
        /// <param name="nodeIdTo">To node id</param>
        /// <returns>
        /// Trail. List of nodes in trail.
        /// </returns>
        public List<string> CalculateShortestPath(string nodeIdFrom, string nodeIdTo)
        {
            Console.WriteLine("WcfGraphService: CalculateShortestPath");

            List<string> retVal = new List<string>();
            try
            {
                ICalculateShortestPathQuery calculate = _container.GetInstance<ICalculateShortestPathQuery>();
                retVal = calculate.CalculateShortestPath(nodeIdFrom, nodeIdTo);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Get nodes from repository.
        /// </summary>
        /// <returns></returns>
        public List<NodeEntity> GetAllNodes()
        {
            Console.WriteLine("WcfGraphService: GetAllNodes");

            List<NodeEntity> retVal = new List<NodeEntity>();
            try
            {
                IGetAllNodesQuery query = _container.GetInstance<IGetAllNodesQuery>();
                retVal = query.GetAllNodes();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Get relations from repository.
        /// </summary>
        /// <returns></returns>
        public List<NodeRelationEntity> GetAllRelations()
        {
            Console.WriteLine("WcfGraphService: GetAllRelations");

            List<NodeRelationEntity> retVal = new List<NodeRelationEntity>();
            try
            {
                IGetAllRelationsQuery query = _container.GetInstance<IGetAllRelationsQuery>();
                retVal = query.GetAllRelations();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return retVal;
        }

        private Container Bootstrap()
        {
            var container = new Container();

            container.Register<IGraphBuilder, Builder>();
            container.Register<IGraphRepository, CodingPlayRepository.CodingPlayRepository>();
            container.Register<ICalculateShortestPathQuery, CalculateShortestPathQuery>();
            container.Register<IGetAllNodesQuery, GetAllNodesQuery>();
            container.Register<IGetAllRelationsQuery, GetAllRelationsQuery>();
            container.Register<IGraphTrailFinder, FindPath>();

            container.Verify();

            return container;
        }
    }
}
