﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphServiceShared;

namespace WcfGraphService
{
    /// <summary>
    /// Query to get all relations from repository.
    /// </summary>
    public class GetAllRelationsQuery : IGetAllRelationsQuery
    {
        private readonly IGraphRepository _repository;

        public GetAllRelationsQuery(IGraphRepository repository)
        {
            _repository = repository;
        }

        public List<NodeRelationEntity> GetAllRelations()
        {
            return _repository.GetAllRelations();
        }
    }
}
