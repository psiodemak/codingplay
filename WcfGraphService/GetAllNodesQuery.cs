﻿using CodingPlayDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphServiceShared;

namespace WcfGraphService
{
    /// <summary>
    /// Query to get all nodes from repository.
    /// </summary>
    public class GetAllNodesQuery : IGetAllNodesQuery
    {
        private readonly IGraphRepository _repository;

        public GetAllNodesQuery(IGraphRepository repository)
        {
            _repository = repository;
        }

        public List<NodeEntity> GetAllNodes()
        {
            return _repository.GetAllNodes();
        }
    }
}
