﻿using GraphProcessor;
using GraphShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfGraphServiceShared;

namespace WcfGraphService
{
    /// <summary>
    /// Query to calculate path between graph nodes.
    /// </summary>
    public class CalculateShortestPathQuery : ICalculateShortestPathQuery
    {
        private IGraphBuilder _graphBuilder;
        private IGraphTrailFinder _trailFinder;

        /// <summary>
        /// Constructore
        /// </summary>
        /// <param name="graphBuilder">Builder of graph</param>
        /// <param name="trailFinder">Trail finder</param>
        public CalculateShortestPathQuery(IGraphBuilder graphBuilder, IGraphTrailFinder trailFinder)
        {
            _graphBuilder = graphBuilder;
            _trailFinder = trailFinder;
        }

        /// <summary>
        /// Calculate path between nodes
        /// 1. Prepare graph
        /// 2. Find trail
        /// 3. Prepare result
        /// </summary>
        /// <param name="nodeIdFrom">From</param>
        /// <param name="nodeIdTo">To</param>
        /// <returns>
        /// Trail. List of nodes in trail.
        /// </returns>
        public List<string> CalculateShortestPath(string nodeIdFrom, string nodeIdTo)
        {
            Graph graph = _graphBuilder.PrepareGraph();

            int nodeFrom = _graphBuilder.GetGraphIdForNodeId(nodeIdFrom);
            int nodeTo = _graphBuilder.GetGraphIdForNodeId(nodeIdTo);

            var trail = _trailFinder.FindTrail(graph, nodeFrom, nodeTo);

            List<string> retVal = ConvertTrail(trail);
            return retVal;
        }

        private List<string> ConvertTrail(List<int> trail)
        {
            List<string> retVal = new List<string>();
            foreach (var graphId in trail)
            {
                string nodeId = _graphBuilder.GetNodeIdForGraphId(graphId);
                retVal.Add(nodeId);
            }

            return retVal;
        }
    }
}
