﻿using CodingPlayDomain;
using GraphShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfDataLoaderShared;

namespace CodingPlayRepository
{
    /// <summary>
    /// Implementation of repository. 
    /// Read write: IDataLoaderReadWriteRepository
    /// Read only: IGraphRepository
    /// Uses Entity Framework to process data.
    /// </summary>
    public class CodingPlayRepository : IDataLoaderReadWriteRepository, IGraphRepository
    {
        private CodingPlayEntities _context;

        /// <summary>
        /// Constructor
        /// </summary>
        public CodingPlayRepository()
        {
            _context = new CodingPlayEntities();
        }

        /// <summary>
        /// All nodes
        /// </summary>
        /// <returns>
        /// List of nodes.
        /// </returns>
        public List<NodeEntity> GetAllNodes()
        {
            var result = (from node in _context.Nodes select new NodeEntity() { NodeId = node.NodeId, Label = node.Label }).ToList();
            return result;
        }

        /// <summary>
        /// All relations.
        /// </summary>
        /// <returns>
        /// List of relations.
        /// </returns>
        public List<NodeRelationEntity> GetAllRelations()
        {
            var result = (from relation in _context.NodeRelations select new NodeRelationEntity() { NodeId = relation.NodeId, AdjacentNodeId = relation.AdjacentNodeId }).ToList();
            return result;
        }

        /// <summary>
        /// Create node in repository
        /// </summary>
        /// <param name="nodeId">Id</param>
        /// <param name="label">Label</param>
        public void AddNode(string nodeId, string label)
        {
            var node = new Node();
            node.NodeId = nodeId;
            node.Label = label;

            _context.Nodes.Add(node);
        }

        /// <summary>
        /// Create relation in repository.
        /// </summary>
        /// <param name="nodeId">Node id</param>
        /// <param name="adjacentNodeId">Adjacent node id</param>
        public void AddRelation(string nodeId, string adjacentNodeId)
        {
            var relation = new NodeRelation();
            relation.NodeId = nodeId;
            relation.AdjacentNodeId = adjacentNodeId;

            _context.NodeRelations.Add(relation);
        }

        /// <summary>
        /// Save data in db
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Delete all existing nodes and relation from db.
        /// </summary>
        public void DeleteAllData()
        {
            _context.Database.ExecuteSqlCommand("delete from [NodesRelations]");
            _context.Database.ExecuteSqlCommand("delete from [Nodes]");
        }
    }
}
